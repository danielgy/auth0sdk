import React, { Component } from 'react';
import {withRouter} from 'react-router';
import {Route} from 'react-router-dom';
import Callback from './Callback';
import './App.css';

function HomePage(props) {
  const {authenticated} = props;

  const logout = () => {
    props.auth.logout();
    props.history.push('/');
  };

  const changePassword = () => {
    props.auth.changePassword();
  }

  if (authenticated) {
    const {name, nickname} = props.auth.getProfile();
    const user = props.auth.getUser();
    return (
      <div>
        <h1>Howdy! Glad to see you back, {name}.</h1>
        <h4>Nickname: {nickname}</h4>
        <code>{JSON.stringify(user, null, 2)}</code><br />
        <button onClick={logout}>Log out</button>
        <button onClick={changePassword}>Reset Password</button>
      </div>
    );
  }

  return (
    <div>
      <h1>I don't know you. Please, log in.</h1>
      <button onClick={props.auth.login}>Log in</button><br />
      <button onClick={props.auth.loginGoogle}>Log in with Google</button><br />
      <button onClick={props.auth.loginFB}>Log in with Facebook</button><br />

      <h2>Sign up</h2>
      <button onClick={props.onClick}>Sign up</button>
    </div>
  );
}

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: "",
      password: "",
    }
  }

  onClick = e => {
    this.props.auth.signup("dpsol@yahoo.com", "zxASqw12");
  }

  render() {
    const authenticated = this.props.auth.isAuthenticated();
    return (
      <div className="App">
        <Route exact path='/callback' render={() => (
          <Callback auth={this.props.auth}/>
        )}/>
        <Route exact path='/' render={() => (
          <HomePage
            authenticated={authenticated}
            auth={this.props.auth}
            history={this.props.history}
            onClick={this.onClick}
          />)
        }/>
      </div>
    );  
  }
}

export default withRouter(App);